package INF102.lab1.triplicate;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class MyTriplicate<T> implements ITriplicate<T> {

    @Override
    public T findTriplicate(List<T> list) {
        int n = list.size();
        ArrayList<T> List = new ArrayList<>();
        ArrayList<T> ListofPairs = new ArrayList<>();
        for (int i = 0; i < n; i++) {
            if (ListofPairs.contains(list.get(i))) {
                T triplet = list.get(i);
                return triplet;
                }
            else if (List.contains(list.get(i))) {
                    ListofPairs.add(list.get(i));
                }
            else {
                List.add(list.get(i));
            }

        }
        return null;
    }

    // @Override
    // public T findTriplicate(List<T> list) {
    // int n = list.size();
    // for (int i = 0; i < n; i++) {
    // for (int j = i+1; j < n; j++) {
    // for (int k = j+1; k < n; k++) {
    // T one = list.get(i);
    // T two = list.get(j);
    // T three = list.get(k);
    // if (one.equals(two) && two.equals(three)) {
    // return one;
    // }
    // }
    // }
    // }
    // return null;
    // }
}
